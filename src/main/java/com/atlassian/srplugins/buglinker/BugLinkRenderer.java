package com.atlassian.srplugins.buglinker;

import com.atlassian.markup.renderer.MarkupRendererComponent;
import com.atlassian.markup.renderer.RenderContext;
import com.atlassian.markup.renderer.RenderTransform;
import com.atlassian.srplugins.logger.PluginLoggerFactory;
//import com.google.common.base.Function;
//import com.atlassian.markup.renderer.transform.TextTransformers;

import javax.annotation.Nonnull;

import org.slf4j.Logger;

//import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class BugLinkRenderer implements MarkupRendererComponent {
	
	private final Logger log;
	PluginLoggerFactory lf = new PluginLoggerFactory();
	
    public BugLinkRenderer() {
    	this.log = lf.getLoggerForThis(this);
    	log.info("************************YOUR LOGGER CREATED**********************");
    }
    
    @Nonnull
    @Override
    public RenderTransform process(CharSequence input, @Nonnull RenderContext context) {
        RenderTransform.TransformBuilder builder = RenderTransform.builder();
        
        Matcher matcher = Pattern.compile("DTS\\d{6}").matcher(input);
        log.info("************************ATTEMPTING TO MATCH**********************");
        while (matcher.find()) {
        	builder.add(matcher.start(), matcher.end(), "<a href=\"http://127.0.0.1:7990/stash\">Found String</a>");
        	log.info("************************FOUND A DTS");        	
        }
        
        return builder.build();
    }
   
/*    public RenderTransform process(CharSequence input, RenderContext context) {
    	log.info("************************ATTEMPTING TO MATCH**********************");
        return TextTransformers.replaceAll(input, Pattern.compile("DTS\\d{6}"), new Function<MatchResult, String>() {
            @Override
            public String apply(MatchResult matcher) {
            	log.info("************************FOUND A DTS");
                //return matcher.group(1).toUpperCase()+
                return "FOUND";
            }
        });
    }
*/
}